# README #


### What is this repository for? ###

This is the one of the component created to be used in AEM application.

The component will list all the pages which are modifed within five days and having the Tags added to them passed by the user in the Touch UI dialog which allows the user to select multiple Tags available across the AEM project.


## How to build

To build all the modules run in the project root directory the following command with Maven 3:

    mvn clean install

If you have a running AEM instance you can build and package the whole project and deploy into AEM with  

    mvn clean install -PautoInstallPackage
    
Or to deploy it to a publish instance, run

    mvn clean install -PautoInstallPackagePublish
    
Or alternatively

    mvn clean install -PautoInstallPackage -Daem.port=4503

Or to deploy only the bundle to the author, run

    mvn clean install -PautoInstallBundle

### Usage guidelines ###

PLease note that you need to create service user to use this component.

1. craete a system user named sysuser or your choice
2. add in the service mapping service like com.aviva.test.core:readService=sysuser in user mapper service
3 add sysuser in default user

To test the component, you can add the component in any parsys by allowing them to be added in the parsys by going into design mode.

1.Select all the tags you wanted to be searched for in the dialog
2. Check the check box if you want to match all the Tags else leave it to match any of the tags.
3.Click Ok.

Note: The pages returned will be only those which are all modified within 5 days from last modified date.

To test the working of this component you can either edit any of the page and select the tags associated with that page.

or also can make use of the Test class provided in which i have edited the page and also tagged some of the tags associated witha t page( content package is also available for the same in this repository).