package com.aviva.test.core.models;

import java.util.HashMap;
import com.adobe.cq.sightly.WCMUse;
import com.aviva.test.core.service.TagInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TagList extends WCMUse {

	Logger logger = LoggerFactory.getLogger(TagList.class);
	private static final String TAGS_Selected = "tagssearched";
	private static final String IS_Checked = "logicaland";
	private boolean logicalAnd = true;
	private String tags[];
	protected HashMap<String, String> pages = new HashMap<String, String>();
	TagInterface service;

	@Override
	public void activate() throws Exception {

		service = getSlingScriptHelper().getService(TagInterface.class);

		if (getProperties().containsKey(IS_Checked)) {
			logicalAnd = getProperties().get(IS_Checked, Boolean.class);

		}
		if (getProperties().containsKey(TAGS_Selected)) {
			tags = getProperties().get(TAGS_Selected, String[].class);
		} else {
			logger.debug("No tags selected");
		}

		logger.debug("checkbox value" + logicalAnd);
		pages = service.getPages(tags, logicalAnd);

	}

	public String[] getSearchTags() {
		return tags;
	}

	public boolean getLogicalAnd() {
		return logicalAnd;
	}

	public HashMap<String, String> getAllPages() {
		return this.pages;
	}

}