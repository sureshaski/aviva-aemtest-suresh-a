package com.aviva.test.core.service.impl;

import com.aviva.test.core.service.TagInterface;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolverFactory;
import com.day.cq.tagging.TagManager;
import com.day.cq.commons.RangeIterator;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import org.apache.sling.api.resource.ResourceResolver;

@Component
@Service
public class TagService implements TagInterface {
	private static final Logger log = LoggerFactory.getLogger(TagService.class);
	private java.util.List<Page> resultPages;
	@Reference
	protected ResourceResolverFactory resolverFactory;

	@Override
	public HashMap<String, String> getPages(String[] tag, boolean matchany) {
		HashMap<String, String> pages = new HashMap<String, String>();

		Iterator<Page> pageIterator = Collections.emptyIterator();

		Map<String, Object> paramMap = new HashMap<String, Object>();
		// Mention the subServiceName used in the User Mapping
		paramMap.put(ResourceResolverFactory.SUBSERVICE, "readService");
		ResourceResolver resolver = null;
		try {
			resolver = resolverFactory.getServiceResourceResolver(paramMap);

			PageManager pageManager = resolver.adaptTo(PageManager.class);

			if (tag.length > 0) {
				TagManager tagManager = resolver.adaptTo(TagManager.class);
				RangeIterator<Resource> rangeIterator = tagManager.find(
						"/content", tag, matchany);
				java.util.List<Page> taggedPages = new ArrayList<Page>();
				while (rangeIterator.hasNext()) {
					Resource r = rangeIterator.next();
					taggedPages.add(pageManager.getContainingPage(r));
				}
				pageIterator = taggedPages.iterator();
			}
			if (!pageIterator.hasNext()) {
				log.debug("Cannot find any elements for this list.");
			} else {
				resultPages = new ArrayList<Page>();
				Calendar cal = Calendar.getInstance();
				while (pageIterator.hasNext()) {
					Page page = pageIterator.next();
					if (modifiedWithinFiveDays(cal, page.getLastModified()))
						resultPages.add(page);
				}

				for (Page p : resultPages) {

					if (modifiedWithinFiveDays(cal, p.getLastModified())) {

						pages.put(p.getPath(), p.getName());
					}
				}
			}
		} catch (Exception e) {
			log.error("unable to execute expiration check", e);
		} finally {
			if (resolver != null) {
				resolver.close();
			}
		}
		return pages;
	}

	boolean modifiedWithinFiveDays(Calendar cal1, Calendar cal2) {
		boolean withinFiveDays = false;
		long diffIndays = Math.abs(cal1.getTimeInMillis()
				- cal2.getTimeInMillis());
		if (diffIndays <= 5 * 24 * 60 * 60 * 1000) {
			withinFiveDays = true;
		}
		return withinFiveDays;
	}

}
