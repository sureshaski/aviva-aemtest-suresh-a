package com.aviva.test.core.models;

import static org.mockito.Mockito.when;
import java.util.HashMap;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.mock;

public class TestTag {

	private TagList tagList;
	private boolean logicalAnd;
	private HashMap<String, String> testPages;

	@Before
	public void setUp() throws Exception {

		logicalAnd = true;
		testPages = new HashMap<String, String>();
		String[] tags = { "geometrixx-outdoors:activity/biking",
				"geometrixx-outdoors:season/summer",
				"geometrixx-outdoors:activity/running" };
		testPages = new HashMap<String, String>();

		// Create a mock of the WCMUse class.
		tagList = mock(TagList.class);
		// Set what to use when superclass methods are called.

		when(tagList.getSearchTags()).thenReturn(tags);
		when(tagList.getLogicalAnd()).thenReturn(logicalAnd);
		when(tagList.getAllPages()).thenReturn(testPages);

	}

	@Test
	public void testgetAllPages() throws Exception {
		testPages.put(
				"content/geometrixx-outdoors/en/activities/cajamara-biking",
				"cajamara-biking");
		tagList.activate();
		HashMap<String, String> allPages = tagList.getAllPages();
		Assert.assertEquals(allPages, testPages);

	}

}